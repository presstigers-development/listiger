<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'presstig_plugin_listiger');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Dq`.R>U/u]_oDWug[Pg|oba}%Cj3 `M)rsf3Zas#W`=d$5AB~RdA%^%97T!:Wl^p');
define('SECURE_AUTH_KEY',  '_z}U|u2p ,yu*#T/)~vrKUVeS&.9)G,ZI738oPSpf#c 1?d:vi_qYCvwZs=D6>-m');
define('LOGGED_IN_KEY',    '#~lO/hr_U`M<iEPc^IAGUSz%@T*3tt^9^9Y/m,Q@qef!J$hCUj-%vpad1k^UKao>');
define('NONCE_KEY',        '`v1<d-Ay@:NA@PgQ0w<&e6G3&/*PS^Sv~Mx6^N4M?f?YaR4jsx@U4tfK?BO99r[j');
define('AUTH_SALT',        'rLEWU^bP1i7@8aNm|=bV?2P#%@XS,2s)uTkr9shslX}+Z9X,S6gJ$a+`j-$hVGK<');
define('SECURE_AUTH_SALT', '}K=AImi.+os-WUvGq/pJU,u>Ppp;RZ:J#VSQVLX|0|a/whasgVwI2|^:+hwk/.f<');
define('LOGGED_IN_SALT',   'u;F*&d]hmWHP%t;GI]B/|gY#3NMA4qR-XTADoVBi/#,]bDUxNH-_P0w5`/k1?|z|');
define('NONCE_SALT',       'Sjw=.$>dGuC6[(D$CfUW zrnMyRN5Im^)5q6:HJ-i;WnwVd/0e@y &_gA,I%-U3-');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'list_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', TRUE);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');