<?php
/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link        http://presstigers.com
 * @since       1.0.0
 *
 * @package     Listiger
 * @subpackage  Listiger/public/partials
 * @author      PressTigers <support@presstigers.com>
 */
?>
<!-- This file should primarily consist of HTML with a little bit of PHP. -->