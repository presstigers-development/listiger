<?php
/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @link        http://presstigers.com
 * @since       1.0.0
 * 
 * @package     Listiger
 * @subpackage  Listiger/includes
 * @author      PressTigers <support@presstigers.com>
 */
class Listiger_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}