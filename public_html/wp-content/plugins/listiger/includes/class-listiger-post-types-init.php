<?php
/**
 * Simple_Listiger_Post_Types_Init Class.
 *
 * Define Custom Post Types.
 * 
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * This is used to define the event_listing custom post type. 
 * This class is used to create custom post type for 'listiger' in admin area.  
 * 
 * @link       http://www.presstigers.com
 * @since      1.0.0
 * 
 * @package    Listiger
 * @subpackage Listiger/includes
 * @author     PressTigers <support@presstigers.com>
 */

class Simple_Listiger_Post_Types_Init {

    /**
     * Initialize the class and set its properties.
     *
     * @since   1.0.0
     * 
     * @return  void
     */
    public function __construct() {
     
        // Including Custom Post Type 'list'
        require_once plugin_dir_path(__FILE__) . 'post-types/class-listiger-list-post-type.php';

        // Check if 'listiger' Post Type Class Exists
        if (class_exists('Simple_Listiger_List_Post_Type')) {
            new Simple_Listiger_List_Post_Type ();
        }
        // Including Custom Post Type 'list_groups' 
        require_once plugin_dir_path(__FILE__) . 'post-types/class-listiger-list-groups-post-type.php';

        // Check if 'list_types' Post Type Class Exists
        if (class_exists('Simple_Listiger_List_groups_Post_Type')) {
            new Simple_Listiger_List_groups_Post_Type ();
        }
    }

}
new Simple_Listiger_Post_Types_Init();