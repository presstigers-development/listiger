<?php
/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 * 
 * @link        http://presstigers.com
 * @since       1.0.0
 * 
 * @package     Listiger
 * @subpackage  Listiger/includes
 * @author      PressTigers <support@presstigers.com>
 */
class Listiger_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}