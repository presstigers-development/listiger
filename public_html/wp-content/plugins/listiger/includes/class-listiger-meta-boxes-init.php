<?php 
/**
 * Simple_Listiger_Meta_Boxes_Init Class.
 *
 * Define Custom Meta Boxes for Custom Post Types.
 * 
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 * 
 * @link       http://www.presstigers.com
 * @since      1.0.0
 * 
 * @package    Listiger
 * @subpackage Listiger/includes
 * @author     PressTigers <support@presstigers.com>
 */
class Simple_Listiger_Meta_Boxes_Init {

    /**
     * Initialize the class and set its properties.
     *
     * @since   1.0.0
     * 
     * @return  void
     */
    public function __construct() {

        // Including Custom Meta Box for POst Type 'listiger'
        require_once plugin_dir_path(__FILE__) . 'meta-boxes/class-listiger-list-meta-box.php';

        // Including Custom Meta Box for POst Type 'list types' 
        require_once plugin_dir_path(__FILE__) . 'meta-boxes/class-listiger-list-groups-meta-box.php';
    }

}
new Simple_Listiger_Meta_Boxes_Init();