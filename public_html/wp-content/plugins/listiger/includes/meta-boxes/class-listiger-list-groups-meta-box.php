<?php if (!defined('ABSPATH')) { exit; } // Exit if accessed directly
/**
 * Simple_Listiger_List_Types_Meta_Box Class.
 *
 * Define Meta box for Custom Post Type 'list_types'. This file 
 * also is used to define add or save post meta of 'list_types'. 
 * 
 * @link       http://www.presstigers.com
 * @since      1.0.0
 * 
 * @package    Listiger
 * @subpackage Listiger/includes/meta-boxes
 * @author     PressTigers <support@presstigers.com>
 */
class Simple_Listiger_List_Groups_Meta_Box {
    /**
     * Initialize the class and set its properties.
     *
     * @since   1.0.0
     * 
     * @return  void
     */
    public function __construct() {
        global $post;
        
        // Action -> Post Type -> list_types -> Create Meta Box.
        add_action('add_meta_boxes', array($this, 'list_groups_create_meta_box'));

        // Action -> Post Type -> list_groups -> Save Meta Box.
        add_action('save_post', array($this, 'list_groups_save_meta_box'));
    }

    /**
     * list_groups_create_meta_box function.
     *
     * @since   1.0.0
     * 
     * @return  void
     */
    public function list_groups_create_meta_box() {
        $this->add_meta_box('list_groups_options', __('List Group Settings', 'listiger'), 'list_groups');
    }
    
    /**
     * add_meta_box function.
     *
     * @since   1.0.0
     *
     * @return  void
     */
    public function add_meta_box($id, $label, $post_type) {
        add_meta_box('_' . $id, $label, array($this, $id), $post_type);
    }
    
    /**
     * list_groups_options function.
     *
     * @since   1.0.0
     * 
     * @return  void
     */
    public function list_groups_options() {                                  
        global $post;
        $list_groups_options = get_post_meta($post->ID, '_list_groups_options', TRUE);
        ?>
        <div class="list-meta-box">
            
            <!-- Location Section -->
            <div class="list-meta-option">
                <label> <?php _e('Location:', 'listiger') ?>  </label>
                <div class="list-meta-option-field">
                    <div class="list-meta-option-checkbox">
                        <label class="list-switch">
                            <input type="checkbox" name="list_options[en_loc]" data-switch="list-location" <?php echo (isset($list_groups_options['en_loc'])) ? "checked" : ''; ?>>
                            <div class="list-slider"></div>
                        </label>
                    </div>
                    <?php $show_hide_class = (isset($list_groups_options['en_loc'])) ? ' list-fieldset-hide' : ''; ?>
                    <fieldset class="list-location<?php echo $show_hide_class;?>">
                        <input type="text" name="list_options[location]" id="list-location" value="<?php echo isset($list_groups_options['location']) ? $list_groups_options['location'] :''; ?>">
                        <span><?php _e('Location Settings', 'listiger') ?></span>
                    </fieldset>
                </div>
            </div>
            
            <!-- User Contact Info Section -->
            <div class="list-meta-option">
                <label> <?php _e('User Contact Info:', 'listiger') ?>  </label>
                <div class="list-meta-option-field">
                    <div class="list-meta-option-checkbox">
                        <label class="list-switch">
                            <input type="checkbox" name="list_options[en_user_info]" data-switch="list-contact-info" <?php echo (isset($list_groups_options['en_user_info'])) ? "checked" : ''; ?> />
                            <div class="list-slider"></div>
                        </label>
                    </div>
                    <?php $show_hide_class = (isset($list_groups_options['en_user_info'])) ? ' list-fieldset-hide' : ''; ?>
                    <fieldset class="list-contact-info<?php echo $show_hide_class;?>">
                        <input type="text" id="list-location" name="list_options[user_info]" value="<?php echo isset($list_groups_options['user_info']) ? $list_groups_options['user_info'] :''; ?>" />
                        <span><?php _e('User Contact Information', 'listiger') ?></span>
                    </fieldset>
                </div>
            </div>
            
            <!-- Opening Hours Section -->
            <div class="list-meta-option">
                <label> <?php _e('Opening Hours:', 'listiger') ?></label>
                <div class="list-meta-option-field">
                    <div class="list-meta-option-checkbox">
                        <label class="list-switch">
                            <input type="checkbox" name="list_options[en_open_hours]" data-switch="list-opening" <?php echo (isset($list_groups_options['en_open_hours'])) ? "checked" : ''; ?>  />
                            <div class="list-slider"></div>
                        </label>
                    </div>
                    <?php $show_hide_class = (isset($list_groups_options['en_user_info'])) ? ' list-fieldset-hide' : ''; ?>
                    <fieldset class="list-opening">
                        <input type="text" id="open-hours" name="list_options[open_hours]" value="<?php echo isset($list_groups_options['open_hours']) ? $list_groups_options['open_hours'] :'';?>" />
                        <span><?php _e('Opening Hours Settings', 'listiger') ?></span>
                    </fieldset>
                </div>
            </div>
            
            <!-- Video Section -->
            <div class="list-meta-option">
                <label> <?php _e('Enable Video:', 'listiger') ?>  </label>
                <div class="list-meta-option-field">
                    <div class="list-meta-option-checkbox">
                        <label class="list-switch">
                            <input type="checkbox" name="list_options[en_video]" data-switch="list-enable-video" <?php echo (isset($list_groups_options['en_video'])) ? "checked" : ''; ?> />
                            <div class="list-slider"></div>
                        </label>
                    </div>
                    <?php $show_hide_class = (isset($list_groups_options['en_video'])) ? ' list-fieldset-hide' : ''; ?>
                    <fieldset class="list-enable-video">
                        <input type="text" id="enable-video" name="list_options[video]" value="<?php echo isset($list_groups_options['video']) ? $list_groups_options['video'] :''; ?>"/>    
                        <span><?php _e('Video Settings', 'listiger') ?></span>
                    </fieldset>
                </div>
            </div>
            
        </div>
   <?php 
    }

    /**
     * list_groups_save_meta_box function.
     *
     * @since   1.0.0
     * 
     * @return  void
     */
    public function list_groups_save_meta_box($post_id) {
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return;
        }
        
        if (!empty($_POST['list_options'])){
        $list_options = $_POST['list_options'];
            update_post_meta($post_id, "_list_groups_options", $list_options);
        }
    }

}
new Simple_Listiger_List_Groups_Meta_Box();