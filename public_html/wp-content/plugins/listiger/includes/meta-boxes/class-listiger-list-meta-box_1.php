<?php
if (!defined('ABSPATH'))
    exit; // Exit if accessed directly
/**
 * Listiger_List_Meta_Box Class.
 *
 * This class is used to create custom meta box for 'listiger'.This file 
 * also is used to define add or save post meta of 'listiger'. 
 * 
 * @link        http://presstigers.com
 * @since       1.0.0
 *
 * @package     Listiger
 * @subpackage  Listiger/includes/meta-boxes
 * @author      PressTigers <support@presstigers.com>
 */

class Listiger_List_Meta_Box {

    /**
     * Initialize the class and set its properties.
     *
     * @since   1.0.0
     */
    public function __construct() {
        global $post;

        // Action -> Post Type -> list -> Create Meta Box for Custom Fields
        add_action('add_meta_boxes', array($this, 'list_create_meta_box'));

        // Action -> Post Type -> list -> Save Meta Box of Custom Fields
        add_action('save_post', array($this, 'list_save_meta_box'));

        // Creating Meta Box for Gallery on Add New List Page
        $this->list_gallery = array(
            'id' => 'list_side_gallery_images',
            'title' => __('Add Gallery Images', 'listiger'),
            'context' => 'side',
            'screen' => 'list',
            'priority' => 'high',
            'callback' => 'list_gallery_output',
            'show_names' => TRUE,
            'closed' => FALSE,
        );

        // Action -> Post Type -> list -> Create Meta Box for Custom Gallery
        add_action('add_meta_boxes', array($this, 'list_gallery_create_meta_box'));

        // Action -> Post Type -> list -> Save Meta Box of Customs Gallery.
        add_action('save_post', array($this, 'list_gallery_save_meta_box'));
    }

    /**
     * list_create_meta_box function.
     *
     * @since   1.0.0
     * 
     * @return  void
     */
    public function list_create_meta_box() {
        $this->add_meta_box('list_options', __('List Options', 'listiger'), 'list');
    }

    /**
     * add_meta_box function.
     *
     * @since   1.0.0
     *
     * @return  void
     */
    public function add_meta_box($id, $label, $post_type) {
        add_meta_box('_' . $id, $label, array($this, $id), $post_type);
    }

    /**
     * list_options function
     *
     * @since   1.0.0
     * 
     * @return  void
     */
    public function list_options() {
        global $post;
        $list_cusotm_fields = get_post_meta($post->ID, "_list_cusotm_fields", true);
        //echo '<pre>'; print_r($list_cusotm_fields);echo '</pre>';
        ?>
        <div class="list-meta-box">
            <!-- Organizer List -->
            <div class="list-meta-option">
                <label><?php _e('Select Organizer', 'listiger'); ?></label>
                <div class="list-meta-option-field">
                    <?php
                    // Get Organizer List
                    $users = get_users('orderby=nicename');
                    echo '<select name="list_cusotm_fields[list_organizer]" id="list_organizer"><option value="">None</option>';
                    foreach ($users as $user) {
                        if ($user->ID == $list_cusotm_fields['list_organizer']) {
                            $selected = ' selected="selected"';
                        } else {
                            $selected = '';
                        }
                        echo '<option value="' . $user->ID . '" ' . $selected . '>' . $user->display_name . '</option>';
                    }
                    echo '</select>';
                    ?>
                    <span><?php _e('Name of organizer of Listings', 'listiger'); ?></span>
                </div>
            </div>
            
            <!-- Group List -->
            <?php
            //Get Custom Post Type "list_groups"
            $list_group_args = array(
                'posts_per_page' => "-1",
                'post_type' => 'list_groups',
                'post_status' => 'publish',
                'orderby' => 'ID',
                'order' => 'ASC',
            );
            $list_groups = get_posts($list_group_args);
            if (is_array($list_groups) && !empty($list_groups)) {
            ?>
            <div class="list-meta-option">
                <label><?php _e('Select List Group', 'listiger'); ?></label>
                <div class="list-meta-option-field">
                    <select name="list_cusotm_fields[list_group_select]" id="list_group_select" onchange="list_group_fields(this.value, '<?php echo esc_js($post->ID); ?>', '<?php echo esc_js(admin_url('admin-ajax.php')); ?>', 'admin')">
                        <?php
                        echo '<option value="">None</option>';
                        foreach ($list_groups as $list_group) :
                        $selected = '';
                        if (isset($list_cusotm_fields['list_group_select']) && $list_cusotm_fields['list_group_select'] == $list_group->ID) {
                        $selected = 'selected';
                        }
                        echo '<option value="' . $list_group->ID . '" ' . $selected . '>' . $list_group->post_title . '</option>';
                        endforeach;
                        wp_reset_postdata();
                        ?>
                    </select>
                </div>
            </div>
            <?php
            $list_group_id = absint($list_group->ID);
            $list_group_postmeta = get_post_meta($list_group_id, '_list_groups_options', TRUE);
           // echo '<pre>'; print_r($list_group_postmeta);echo '</pre>';
            
            // Check If a list group ID exists.
            $display = (isset($list_cusotm_fields['list_group_select'])) ? 'display: block;' : 'display: none;';
            ?>
            <div id="group_options" style="<?php echo $display; ?>">
                
                <!-- Video Section -->
                <?php if (array_key_exists('en_video', $list_group_postmeta)) { ?>
                <div class="list-section"><?php echo __('Video', 'listiger'); ?></div>
                <div class="list-meta-option">
                    <label><?php echo __('Video URL', 'listiger'); ?></label>
                    <div class="list-meta-option-field">
                        <input type="url" name="list_cusotm_fields[video_url]" value="<?php echo isset($list_cusotm_fields['video_url']) ? $list_cusotm_fields['video_url'] :'';  ?>" placeholder="URL">
                        <span><?php echo __('You can add Youtube, Vimeo, Dailymotion Videos URL etc', 'listiger'); ?></span>
                    </div>
                </div>
                <?php } //End Video Section ?>
                
                <!-- Services Opening Hours Section -->
                <?php if (array_key_exists('op_hours', $list_group_postmeta)) { ?>
                <div class="list-section"><span>Opening Hours</span></div>
                <div class="list-meta-option">
                    <label><?php echo __('Opening Hours', 'listiger'); ?></label>
                    <div class="list-opening-hours">
                        <div class="list-time-group">
                            <div class="list-day-name">
                                <label> <?php _e('Day Name', 'listiger'); ?> </label>
                                <input type="text" name="list_cusotm_fields[sun]" value="<?php echo isset($list_cusotm_fields['sun']) ? $list_cusotm_fields['sun'] :'';  ?>"  placeholder="Sunday">
                                <input type="text" name="list_cusotm_fields[mon]" value="<?php echo isset($list_cusotm_fields['mon']) ? $list_cusotm_fields['mon'] :'';  ?>"  placeholder="Monday">
                                <input type="text" name="list_cusotm_fields[tue]" value="<?php echo isset($list_cusotm_fields['tue']) ? $list_cusotm_fields['tue'] :'';  ?>"  placeholder="Tuesday">
                                <input type="text" name="list_cusotm_fields[wed]" value="<?php echo isset($list_cusotm_fields['wed']) ? $list_cusotm_fields['wed'] :'';  ?>"  placeholder="Wednesday">
                                <input type="text" name="list_cusotm_fields[thu]" value="<?php echo isset($list_cusotm_fields['thu']) ? $list_cusotm_fields['thu'] :'';  ?>"  placeholder="Thursday">
                                <input type="text" name="list_cusotm_fields[fri]" value="<?php echo isset($list_cusotm_fields['fri']) ? $list_cusotm_fields['fri'] :'';  ?>"  placeholder="Friday">
                                <input type="text" name="list_cusotm_fields[sat]" value="<?php echo isset($list_cusotm_fields['sat']) ? $list_cusotm_fields['sat'] :'';  ?>"  placeholder="Saturday">
                            </div>
                            <div class="list-start-time">
                                <label> <?php _e('Start Time', 'listiger'); ?> </label>
                                <input type="time" name="list_cusotm_fields[sun_start_time]" value="<?php echo isset($list_cusotm_fields['sun_start_time']) ? $list_cusotm_fields['sun_start_time'] :'';  ?>" placeholder="4:00 am">
                                <input type="time" name="list_cusotm_fields[mon_start_time]" value="<?php echo isset($list_cusotm_fields['mon_start_time']) ? $list_cusotm_fields['mon_start_time'] :'';  ?>" placeholder="4:00 am">
                                <input type="time" name="list_cusotm_fields[tue_start_time]" value="<?php echo isset($list_cusotm_fields['tue_start_time']) ? $list_cusotm_fields['tue_start_time'] :'';  ?>" placeholder="4:00 am">
                                <input type="time" name="list_cusotm_fields[wed_start_time]" value="<?php echo isset($list_cusotm_fields['wed_start_time']) ? $list_cusotm_fields['wed_start_time'] :'';  ?>" placeholder="4:00 am">
                                <input type="time" name="list_cusotm_fields[thu_start_time]" value="<?php echo isset($list_cusotm_fields['thu_start_time']) ? $list_cusotm_fields['thu_start_time'] :'';  ?>" placeholder="4:00 am">
                                <input type="time" name="list_cusotm_fields[fri_start_time]" value="<?php echo isset($list_cusotm_fields['fri_start_time']) ? $list_cusotm_fields['fri_start_time'] :'';  ?>" placeholder="4:00 am">
                                <input type="time" name="list_cusotm_fields[sat_start_time]" value="<?php echo isset($list_cusotm_fields['sat_start_time']) ? $list_cusotm_fields['sat_start_time'] :'';  ?>" placeholder="4:00 am">
                            </div>
                            <div class="list-end-time">
                                <label> <?php _e('End Time', 'listiger'); ?></label>
                                <input type="time" name="list_cusotm_fields[sun_end_time]" value="<?php echo isset($list_cusotm_fields['sun_end_time']) ? $list_cusotm_fields['sun_end_time'] :'';  ?>" placeholder="6:00 pm">
                                <input type="time" name="list_cusotm_fields[mon_end_time]" value="<?php echo isset($list_cusotm_fields['mon_end_time']) ? $list_cusotm_fields['mon_end_time'] :'';  ?>" placeholder="6:00 pm">
                                <input type="time" name="list_cusotm_fields[tue_end_time]" value="<?php echo isset($list_cusotm_fields['tue_end_time']) ? $list_cusotm_fields['tue_end_time'] :'';  ?>" placeholder="6:00 pm">
                                <input type="time" name="list_cusotm_fields[wed_end_time]" value="<?php echo isset($list_cusotm_fields['wed_end_time']) ? $list_cusotm_fields['wed_end_time'] :'';  ?>" placeholder="6:00 pm">
                                <input type="time" name="list_cusotm_fields[thu_end_time]" value="<?php echo isset($list_cusotm_fields['thu_end_time']) ? $list_cusotm_fields['thu_end_time'] :'';  ?>" placeholder="6:00 pm">
                                <input type="time" name="list_cusotm_fields[fri_end_time]" value="<?php echo isset($list_cusotm_fields['fri_end_time']) ? $list_cusotm_fields['fri_end_time'] :'';  ?>" placeholder="6:00 pm">
                                <input type="time" name="list_cusotm_fields[sat_end_time]" value="<?php echo isset($list_cusotm_fields['sat_end_time']) ? $list_cusotm_fields['sat_end_time'] :'';  ?>" placeholder="6:00 pm">
                            </div>
                        </div>
                    </div>
                </div>
                <?php } //End Services Opening Hours Section ?>
                
                <!-- Contact information Section -->
                <?php if (array_key_exists('cont_info', $list_group_postmeta)) { ?>
                <div class="list-section"><span> <?php _e('Contact Information', 'listiger'); ?> </span></div>
                <div class="list-meta-option">
                    <label><?php _e('Landline', 'listiger'); ?></label>
                    <div class="list-meta-option-field">
                        <input type="tel" name="list_cusotm_fields[landline]" value="<?php echo isset($list_cusotm_fields['landline']) ? $list_cusotm_fields['landline'] :'';  ?>" placeholder="123 500 500 500">
                    </div>
                </div>
                <div class="list-meta-option">
                    <label><?php _e('Mobile', 'listiger'); ?></label>
                    <div class="list-meta-option-field">
                        <input type="tel" name="list_cusotm_fields[mobile]" value="<?php echo isset($list_cusotm_fields['mobile']) ? $list_cusotm_fields['mobile'] :'';  ?>" placeholder="120 500 500 500">
                    </div>
                </div>
                <div class="list-meta-option">
                    <label><?php _e('Email Address', 'listiger'); ?></label>
                    <div class="list-meta-option-field">
                        <input type="email" name="list_cusotm_fields[mail]" value="<?php echo isset($list_cusotm_fields['mail']) ? $list_cusotm_fields['mail'] :'';  ?>" placeholder="info@presstigers.com">
                    </div>
                </div>
                <div class="list-meta-option">
                    <label> <?php _e('Skype', 'listiger'); ?> </label>
                    <div class="list-meta-option-field">
                        <input type="text" name="list_cusotm_fields[skype]" value="<?php echo isset($list_cusotm_fields['skype']) ? $list_cusotm_fields['skype'] :'';  ?>" placeholder="presstigers">
                    </div>
                </div>
                <div class="list-meta-option">
                    <label> <?php _e('Webiste', 'listiger'); ?> </label>
                    <div class="list-meta-option-field">
                        <input type="url" name="list_cusotm_fields[website]" value="<?php echo isset($list_cusotm_fields['website']) ? $list_cusotm_fields['website'] :'';  ?>" placeholder="URL">
                    </div>
                </div>
                <?php } //End Contact information Section ?>
                
                <!-- Location Section -->
                <?php if (array_key_exists('loc', $list_group_postmeta)) { ?>
                <div class="list-section"><span> <?php _e('Locations', 'listiger'); ?> </span></div>
                <div class="list-meta-option">
                    <label><?php _e('Country', 'listiger'); ?> </label>
                    <div class="list-meta-option-field">
                        <select name="country" class="countries" id="countryId">
                           <option value="">Select Country</option>
                        </select>
                    </div>
                </div>
                <div class="list-meta-option">
                    <label><?php _e('State', 'listiger'); ?> </label>
                    <div class="list-meta-option-field">
                       <select name="state" class="states" id="stateId">
                            <option value="">Select State</option>
                        </select>
                    </div>
                </div>
                <div class="list-meta-option">
                    <label><?php _e('City', 'listiger'); ?></label>
                    <div class="list-meta-option-field">
                        <select name="city" class="cities" id="cityId">
                            <option value="">Select City</option>
                        </select>
                    </div>
                </div>
                <div class="list-meta-option">
                    <label><?php _e('Complete Address', 'listiger'); ?> </label>
                    <div class="list-meta-option-field">
                        <textarea name="list_cusotm_fields[address]" value="<?php echo isset($list_cusotm_fields['address']) ? $list_cusotm_fields['address'] :'';  ?>" > </textarea>
                    </div>
                </div>
                <div class="list-meta-option">
                    <label><?php _e('latitude', 'listiger'); ?> </label>
                    <div class="list-meta-option-field">
                        <input type="text" name="list_cusotm_fields[lat]" value="<?php echo isset($list_cusotm_fields['lat']) ? $list_cusotm_fields['lat'] :'';  ?>" placeholder="41.857651">
                    </div>
                </div>
                <div class="list-meta-option">
                    <label><?php _e('Longitude', 'listiger'); ?></label>
                    <div class="list-meta-option-field">
                        <input type="text" name="list_cusotm_fields[long]" value="<?php echo isset($list_cusotm_fields['long']) ? $list_cusotm_fields['long'] :'';  ?>" placeholder="-72.343557">
                    </div>
                </div>
                <div class="list-meta-option">
                    <label>&nbsp;</label>
                    <div class="list-meta-option-field">
                        <button><?php _e('Search This Location on Map', 'listiger'); ?></button>
                    </div>
                </div>
                <div class="list-meta-option">
                    <!-- Google Map -->
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d5943.265588160738!2d-72.343514!3d41.85773!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89e6f4b82e8fc83b%3A0x190c4dc9d404f91c!2s1+Technology+Drive%2C+Tolland%2C+CT+06084!5e0!3m2!1sen!2sus!4v1471958862869" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <?php } //End Location Section ?>
            </div>
            <?php }?>
        </div>
   <?php
   }
   
   /**
     * list_save_meta_box function.
     *
     * @since   1.0.0
     * 
     * @return  void
     */
    public function list_save_meta_box($post_id) {
        global $post;
        
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return;
        }

        // Get list_cusotm_fields Array
       //$list_post_meta = isset($_POST['list_cusotm_fields']) ? $_POST['list_cusotm_fields'] : '';
        if(isset($_POST['list_cusotm_fields'])){
            update_post_meta($post->ID, '_list_cusotm_fields', $list_post_meta);
        }
        //wp_die('stop');
    }

    /**
     * Getter of 'get_listiger_gallery' meta box.
     *
     * @since   1.0.0
     */
    public function get_list_gallery() {
        return $this->list_gallery;
    }

    /**
     * listiger_gallery_create_meta_box funtion.
     *
     * @since   1.0.0 
     */
    public function list_gallery_create_meta_box() {
        $list_gallery = self::get_list_gallery();
        add_meta_box($list_gallery['id'], $list_gallery['title'], array($this, $list_gallery['callback']), $list_gallery['screen'], $list_gallery['context'], $list_gallery['priority']);
    }

    /**
     * listiger_gallery_output function.
     *
     * @since   1.0.0 
     * 
     * @param   object  $post   Post Object
     */
    public function list_gallery_output($post) {
        global $post;

        // Add a nonce field so we can check it for later.
        wp_nonce_field('list_meta_box', 'list_images_meta_box_nonce');
        ?>

        <!-- Gallery Images -->
        <div id="list-gallery-container">
            <ul class="list-images">
                <?php
                if (metadata_exists('post', $post->ID, '_list_gallery')) {
                    $list_gallery_image = get_post_meta($post->ID, '_list_gallery', TRUE);
                } else {
                    $attachment_ids = get_posts(
                            'post_parent=' . $post->ID . '&'
                            . 'numberposts=-1&'
                            . 'post_type=attachment&'
                            . 'orderby=menu_order&'
                            . 'order=ASC&'
                            . 'post_mime_type=image&'
                            . 'fields=ids&'
                    );
                    $attachment_ids = array_diff($attachment_ids, array(get_post_thumbnail_id()));
                    $list_gallery_image = implode(',', $attachment_ids);
                }

                $attachments = array_filter(explode(',', $list_gallery_image));
                $update_meta = FALSE;
                $updated_gallery_ids = array();

                if (!empty($attachments)) {
                    foreach ($attachments as $attachment_id) {
                        $attachment = wp_get_attachment_image($attachment_id, 'thumbnail');
                        $alt = get_post_meta($attachment_id, '_wp_attachment_image_alt', true);
                        $attachment_meta = get_post($attachment_id);

                        // Skip Empty Attachment
                        if (empty($attachment)) {
                            $update_meta = TRUE;
                            continue;
                        }

                        echo '<li class="image" data-attachment_id="' . esc_attr($attachment_id) . '">
                                ' . $attachment . '<br>
                                <a href="#" class="delete tips" data-tip="' . esc_attr__('Delete Slide', 'listiger') . '"><i class="fa fa-times" aria-hidden="true"></i>
                              </a>
                        </li>';


                        // Rebuild IDs to be Saved
                        $updated_gallery_ids[] = $attachment_id;
                    }

                    // Update Soc Slider Meta to Set New Slide's IDs
                    if ($update_meta) {
                        update_post_meta($post->ID, '_list_gallery', implode(',', $updated_gallery_ids));
                    }
                }
                ?>
            </ul>
            <input type="hidden" id="list_gallery_images" name="list_gallery_images" value="<?php echo esc_attr($list_gallery_image); ?>" />
        </div>
        <p class="add_image hide-if-no-js">
            <a href="#" data-choose="<?php esc_attr_e('Add Image to Gallery', 'listiger'); ?>" data-update="<?php esc_attr_e('Add to Gallery', 'listiger'); ?>" data-delete="<?php esc_attr_e('Delete Image', 'listiger'); ?>" data-text="<?php esc_attr_e('Delete', 'listiger'); ?>"><?php _e('Add Image to Gallery', 'listiger'); ?></a>
        </p>
        <?php
    }

    /**
     * listiger_gallery_save_meta_box funtion.
     *
     * @since   1.0.0
     */
    public static function list_gallery_save_meta_box() {
        global $post;

        // Check Nonce Field
        if (!isset($_POST['list_images_meta_box_nonce'])) {
            return;
        }

        // Verify that the nonce is valid.
        if (!wp_verify_nonce($_POST['list_images_meta_box_nonce'], 'list_meta_box')) {
            return;
        }

        // If this is an autosave, our form has not been submitted, so we don't want to do anything.
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return;
        }

        // Check the user's permissions.
        if (isset($_POST['post_type']) && 'page' == $_POST['post_type']) {
            if (!current_user_can('edit_page', $post->ID)) {
                return;
            }
        } else {
            if (!current_user_can('edit_post', $post->ID)) {
                return;
            }
        }

        // Get Attachment's/Image's IDs
        $attachment_ids = isset($_POST['list_gallery_images']) ? array_filter(explode(',', $_POST['list_gallery_images'])) : array();
        update_post_meta($post->ID, '_list_gallery', implode(',', $attachment_ids));
    }

}

new Listiger_List_Meta_Box();