<?php if (!defined('ABSPATH')) { exit; } // Exit if accessed directly
/**
 * Simple_Listiger_List_Post_Type Class.
 *
 * This is used to define the 'list' custom post type. 
 * This class is used to create custom post type for listiger in admin area. 
 * 
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area. 
 * 
 * @link        http://www.presstigers.com
 * @since       1.0.0
 * 
 * @package     Listiger
 * @subpackage  Listiger/includes
 * @author      PressTigers <support@presstigers.com>
 */
class Simple_Listiger_List_Post_Type {

    /**
     * Initialize the class and set its properties.
     *
     * @since   1.0.0
     * 
     * @return  void
     */
    public function __construct() {

        // Add Hook into the 'init()' action
        add_action('init', array($this, 'list_init'));

        // Add Hook into the 'init()' action
        add_action('init', array($this, 'taxonomy_init'));

        // Add Hook into the 'admin_init()' action
        //add_action('admin_init', array($this, 'listiger_admin_init'));
    }

    /**
     * A function hook that the WordPress core launches at 'init' points
     *          
     * @since   1.0.0
     */
    public function list_init() {
        $this->createPostType();

        // Flush Rewrite Rules
        flush_rewrite_rules();
    }

    /**
     * Register Custom Post Type 'list' 
     *
     * @since   1.0.0
     */
    public function createPostType() {

        if (post_type_exists("list")) {
            return;
        }

        /**
         * Post Type -> List
         */
        $singular = __('List', 'listiger');
        $plural = __('Lists', 'listiger');

        $rewrite = array(
            'slug' => _x('listiger', 'Listiger permalink - resave permalinks after changing this', 'listiger'),
            'with_front' => FALSE,
            'feeds' => FALSE,
            'pages' => FALSE,
            'hierarchical' => FALSE,
        );

        // Post Type -> List -> Label Arguments
        $list_labels = array(
            'name' => $plural,
            'singular_name' => $singular,
            'menu_name' => __('LisTiger', 'listiger'),
            'all_items' => sprintf(__('%s', 'listiger'), $plural),
            'add_new' => sprintf(__('Add New %s', 'listiger'), $singular),
            'add_new_item' => sprintf(__('Add New %s', 'listiger'), $singular),
            'edit' => __('Edit', 'listiger'),
            'edit_item' => sprintf(__('Edit %s', 'listiger'), $singular),
            'new_item' => sprintf(__('New %s', 'listiger'), $singular),
            'view' => sprintf(__('View %s', 'listiger'), $singular),
            'view_item' => sprintf(__('View %s', 'listiger'), $singular),
            'search_items' => sprintf(__('Search %s', 'listiger'), $plural),
            'not_found' => sprintf(__('No %s found', 'listiger'), $plural),
            'not_found_in_trash' => sprintf(__('No %s found in trash', 'listiger'), $plural),
            'parent' => sprintf(__('Parent %s', 'listiger'), $singular)
        );

        // Post Type -> List -> List Arguments
        $list_arguments = array(
            'labels' => $list_labels,
            'description' => sprintf(__('This is where you can create and manage %s.', 'listiger'), $plural),
            'public' => TRUE,
            'show_ui' => TRUE,
            'capability_type' => 'post',
            'map_meta_cap' => TRUE,
            'publicly_queryable' => TRUE,
            'exclude_from_search' => FALSE,
            'hierarchical' => FALSE,
            'rewrite' => $rewrite,
            'query_var' => TRUE,
            'can_export' => TRUE,
            'supports' => array(
                'title',
                'editor',
                'excerpt',
                'author',
                'comments',
                'thumbnail',
                'page-attributes',
            ),
            'has_archive' => TRUE,
            'show_in_nav_menus' => TRUE,
            'menu_icon' => 'dashicons-menu',
        );

        // Register List Post Type
        register_post_type("list", apply_filters("register_post_type_list", $list_arguments));
    }

    /**
     * Hook into WP's init action hook
     *
     * @since   1.0.0
     */
    public function taxonomy_init() {

        // Initialize Custom Taxonomy for Post Type List 
        $this->list_register_categories();
        $this->list_register_locations();
        $this->list_register_tags();

    }

    /*
     * Create Categories for Custom Post Type List
     * 
     * @since   1.0.0
     */
    public function list_register_categories() {

        /* Custom Taxonomy for Custom Post Type 'list' */
        $singular = __('Category', 'listiger');
        $plural = __('Categories', 'listiger');

        if (current_theme_supports('list-templates')) {
            $rewrite = array(
                'slug' => _x('list-category', 'Listiger category slug - resave permalinks after changing this', 'listiger'),
                'with_front' => FALSE,
                'hierarchical' => FALSE,
            );

            $public = TRUE;
        } else {
            $rewrite = FALSE;
            $public = FALSE;
        }

        // Post Type -> List -> Taxonomy -> List Category -> Labels
        $labels_category = array(
            'name' => __('List Categories', 'listiger'),
            'singular_name' => $singular,
            'menu_name' => ucwords($plural),
            'search_items' => sprintf(__('Search %s', 'listiger'), $plural),
            'all_items' => sprintf(__('All %s', 'listiger'), $plural),
            'parent_item' => sprintf(__('Parent %s', 'listiger'), $singular),
            'parent_item_colon' => sprintf(__('Parent %s:', 'listiger'), $singular),
            'edit_item' => sprintf(__('Edit %s', 'listiger'), $singular),
            'update_item' => sprintf(__('Update %s', 'listiger'), $singular),
            'add_new_item' => sprintf(__('Add New %s', 'listiger'), $singular),
            'new_item_name' => sprintf(__('New %s Name', 'listiger'), $singular)
        );

        // Post Type -> List -> Taxonomy -> List Category -> Arguments
        $args_category = array(
            'hierarchical' => TRUE,
            'update_count_callback' => '_update_post_term_count',
            'label' => $plural,
            'labels' => $labels_category,
            'show_ui' => TRUE,
            'public' => $public,
            'rewrite' => FALSE,
        );

        // Register List Categry Taxonomy
        register_taxonomy(
                "list_category", apply_filters('register_taxonomy_list_category_object_type', array('list')), apply_filters('register_taxonomy_list_category_args', $args_category)
        );
    }
    /*
     * Create Loactaions for Custom Post Type 'list'
     * 
     * @since  1.0.0
     */

    public function list_register_locations() {

        $location_labels = array(
            'name' => __('List Locations', 'listiger'),
            'search_items' => __('Search Locations', 'listiger'),
            'popular_items' => 'Popular Locations',
            'all_items' => __('All Locations', 'listiger'),
            'parent_item' => null,
            'parent_item_colon' => null,
            'edit_item' => __('Edit Location', 'listiger'),
            'update_item' => __('Update Location', 'listiger'),
            'add_new_item' => __('Add New Location', 'listiger'),
            'new_item_name' => __('New Location Name', 'listiger'),
            'separate_items_with_commas' => __('Separate locations with commas', 'listiger'),
            'add_or_remove_items' => __('Add or remove locations', 'listiger'),
            'choose_from_most_used' => __('Choose from the most used locations', 'listiger'),
            'menu_name' => __('Locations', 'listiger'),
        );

        register_taxonomy("list-location", 'list', array(
            'hierarchical' => false,
            'labels' => $location_labels,
            'show_ui' => true,
            'update_count_callback' => '_update_post_term_count',
            'query_var' => true,
            'rewrite' => array('slug' => "list-location"),
        ));
    }

    /*
     * Create Tags for Custom Post Type 'list'
     * 
     * @since  1.0.0
     */
    public function list_register_tags() {

        $labels_tags = array(
            'name' => __('List Tags', 'listiger'),
            'search_items' => __('Search Tags', 'listiger'),
            'popular_items' => 'Popular Tags',
            'all_items' => __('All Tags', 'listiger'),
            'parent_item' => null,
            'parent_item_colon' => null,
            'edit_item' => __('Edit Tag', 'listiger'),
            'update_item' => __('Update Tag', 'listiger'),
            'add_new_item' => __('Add New Tag', 'listiger'),
            'new_item_name' => __('New Tag Name', 'listiger'),
            'separate_items_with_commas' => __('Separate tags with commas', 'listiger'),
            'add_or_remove_items' => __('Add or remove tags', 'listiger'),
            'choose_from_most_used' => __('Choose from the most used tags', 'listiger'),
            'menu_name' => __('Tags', 'listiger'),
        );
        register_taxonomy("list-tag", 'list', array(
            'hierarchical' => false,
            'labels' => $labels_tags,
            'show_ui' => true,
            'update_count_callback' => '_update_post_term_count',
            'query_var' => true,
            'rewrite' => array('slug' => "list-tag"),
        ));
    }
}