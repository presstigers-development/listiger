<?php if (!defined('ABSPATH')) { exit; } // Exit if accessed directly
/**
 * Simple_Listiger_List_groups_Post_Type Class.
 *
 * This is used to define the list_types custom post type. 
 * This class is used to create custom post type for listiger in admin area.  
 * 
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 * 
 * @link       http://www.presstigers.com
 * @since      1.0.0
 * 
 * @package     Listiger
 * @subpackage  Listiger/includes/post-types
 * @author      PressTigers <support@presstigers.com>
 */
class Simple_Listiger_List_groups_Post_Type {

    /**
     * Initialize the class and set its properties.
     *
     * @since   1.0.0
     * 
     * @return  void
     */
    public function __construct() {

        // Add Hook into the 'init()' action
        add_action('init', array($this, 'list_groups_init'));
    }

    /**
     * A function hook that the WordPress core launches at 'init' points
     *          
     * @since   1.0.0
     */
    public function list_groups_init() {
        $this->createPostType();

        // Flush Rewrite Rules
        flush_rewrite_rules();
    }

    /**
     * Register Post Type & Taxonomy function.
     *
     * @since   1.0.0
     */
    public function createPostType() {

        if (post_type_exists("list_groups")) {
            return;
        }

        register_post_type('list_groups', array(
            'labels' => array(
                'name' => __('List Groups', 'listiger'),
                'singular_name' => __('List Groups', 'listiger'),
                'menu_name' => __('List Groups', 'listiger'),
                'add_new' => __('Add List Group', 'listiger'),
                'add_new_item' => __('Add New List Group', 'listiger'),
                'edit' => __('Edit', 'listiger'),
                'edit_item' => __('Edit List Group', 'listiger'),
                'new_item' => __('New List Group', 'listiger'),
                'view' => __('View List Group', 'listiger'),
                'view_item' => __('View List Group', 'listiger'),
                'search_items' => __('Search List Group', 'listiger'),
                'not_found' => __('No List Group found', 'listiger'),
                'not_found_in_trash' => __('No List Group found in trash', 'listiger'),
                'parent' => __('Parent List Group', 'listiger')
            ),
            'description' => __('This is where you can add new list group.', 'listiger'),
            'public' => false,
            'show_ui' => true,
            'capability_type' => 'post',
            'show_in_menu' => 'edit.php?post_type=list',
            'map_meta_cap' => true,
            'publicly_queryable' => true,
            'exclude_from_search' => false,
            'hierarchical' => false,
            'rewrite' => false,
            'query_var' => true,
            'supports' => array('title'),
            'has_archive' => false,
                )
        );
    }

}
new Simple_Listiger_List_groups_Post_Type();