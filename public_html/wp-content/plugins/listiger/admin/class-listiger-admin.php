<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @link       http://presstigers.com
 * @since      1.0.0
 * 
 * @package    Listiger
 * @subpackage Listiger/admin
 * @author     PressTigers <support@presstigers.com>
 */
class Listiger_Admin {

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of this plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version) {

        $this->plugin_name = $plugin_name;
        $this->version = $version;

        /**
         * The class is responsible for defining the Meta Boxes.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-listiger-meta-boxes-init.php';
    }

    /**
     * Register the stylesheets for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_styles() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Listiger_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Listiger_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/listiger-admin.css', array(), $this->version, 'all');
    }

    /**
     * Register the JavaScript for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts()
    {
        //wp_enqueue_script('jquery.min.1.11.1', 'https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js', '', '1.11.1', FALSE);
        wp_enqueue_script('list-loction', plugin_dir_url(__FILE__) . 'js/location.js', array('jquery'), $this->version, TRUE);
        wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/listiger-admin.js', array('jquery'), $this->version, TRUE);
    }
}
