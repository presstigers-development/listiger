/* 
 *  LisTiger JS File
 *  v1.0.0
 */
(function ($) {
    'use strict';
    
    window.ajaxCall = function ()
    {
        this.send = function (data, url, method, success, type) {
            type = type || 'json';
            
            var successRes = function (data) {
                success(data);
            }
            var errorRes = function (e) {
                console.log(e);
            }
            $.ajax({
                url: url,
                type: method,
                data: data,
                success: successRes,
                error: errorRes,
                dataType: type,
                timeout: 60000
            });

        }

    }

    window.locationInfo = function ()
    {
        var rootUrl = "http://iamrohit.in/lab/php_ajax_country_state_city_dropdown/api.php";
        var call = new ajaxCall();
        
        this.getCities = function (id, city_value)
        {
            $(".cities option:gt(0)").remove();
            var url = rootUrl + '?type=getCities&stateId=' + id;
            var method = "post";
            var data = {};
            $('.cities').find("option:eq(0)").html("Please wait..");
            call.send(data, url, method, function (data) {
                $('.cities').find("option:eq(0)").html("Select City");
                if (data.tp == 1) {
                    $.each(data['result'], function (key, val) {
                        var option = $('<option />');
                        option.attr('value', val).text(val);
                        option.attr('cityid', key);
                        
                        if( val == city_value ) {
                            option.attr('selected', 'selected');
                        }
                        
                        $('.cities').append(option);
                    });
                    $(".cities").prop("disabled", false);
                }
                else {
                    alert(data.msg);
                }
            });
        };

        this.getStates = function (id, state_value)
        {
            $(".states option:gt(0)").remove();
            $(".cities option:gt(0)").remove();
            var url = rootUrl + '?type=getStates&countryId=' + id;
            var method = "post";
            var data = {};
            $('.states').find("option:eq(0)").html("Please wait..");
            call.send(data, url, method, function (data) {
                $('.states').find("option:eq(0)").html("Select State");
                if (data.tp == 1) {
                    $.each(data['result'], function (key, val) {
                        var option = $('<option />');
                        option.attr('value', val).text(val);
                        option.attr('stateid', key);
                        
                        if( val == state_value ) {
                            option.attr('selected', 'selected');
                        }
                        
                        $('.states').append(option);
                    });
                    $(".states").prop("disabled", false);
                }
                else {
                    alert(data.msg);
                }
            });
        };

        this.getCountries = function (country_value)
        {
            var url = rootUrl + '?type=getCountries';
            var method = "post";
            var data = {};
            $('.countries').find("option:eq(0)").html("Please wait..");
            
            call.send(data, url, method, function (data) {
                
                $('.countries').find("option:eq(0)").html("Select Country");
                if (data.tp == 1) {
                    $.each(data['result'], function (key, val) {
                        var option = $('<option />');
                        option.attr('value', val).text(val);
                        option.attr('countryid', key);
                        
                        if( val == country_value ) {
                            option.attr('selected', 'selected');
                        }
                        
                        $('.countries').append(option);
                    });
                    $(".countries").prop("disabled", false);
                }
                else {
                    alert(data.msg);
                }
            });
        };

    }

    $(function ()
    {
        var loc = new locationInfo();
        if (typeof list_location !== 'undefined') {
            loc.getCountries(list_location.country);
            
            $( document ).ajaxComplete(function( event, request, settings ) {
                var res_text = request.responseText;
                var json_obj = $.parseJSON(res_text);//parse JSON
                
                if( json_obj.msg == "Countries fetched successfully." ) {
                    var countryId = $( ".countries option:selected" ).attr('countryid');
                    loc.getStates(countryId, list_location.state);
                    
                    $( document ).ajaxComplete(function( event, request, settings ) {
                        var state_obj = $.parseJSON(request.responseText);//parse JSON
                        if( state_obj.msg == "States fetched successfully." ) {
                            
                            var stateId = $( ".states option:selected" ).attr('stateid');
                            loc.getCities(stateId, list_location.city);
                        }
                    });
                }
                
            });
        } else {
            loc.getCountries();
        }
        
        $(".countries").on("change", function (ev) {
            var countryId = $("option:selected", this).attr('countryid');
            if (countryId != '') {
                loc.getStates(countryId);
            } else {
                $(".states option:gt(0)").remove();
            }
        });
        $(".states").on("change", function (ev) {
            var stateId = $("option:selected", this).attr('stateid');
            if (stateId != '') {
                loc.getCities(stateId);
            } else {
                $(".cities option:gt(0)").remove();
            }
        });
    });
})(jQuery);