/* 
 *  LisTiger JS File
 *  v1.0.0
 */
(function ($) {
    'use strict';

    $(document).ready(function () {
        
        //Gallery Images File Uploader
        var list_gallery_frame;
        var $images_id = $('#list_gallery_images');
        var $list_images_container = $('#list-gallery-container');
        var $list_images = $list_images_container.find('ul.list-images');
        
        if($list_images.length > 0) {

            $('.add_image').on('click', 'a', function (event)
            {
                var $el = $(this);

                event.preventDefault();

                // If the media frame already exists, reopen it.
                if (list_gallery_frame) {
                    list_gallery_frame.open();
                    return;
                }

                // Create Media Frames
                list_gallery_frame = wp.media.frames.list_gallery_frame = wp.media({
                    // Set the title of the modal.
                    title: $el.data('choose'),
                    button: {
                        text: $el.data('update')
                    },
                    states: [
                        new wp.media.controller.Library({
                            title: $el.data('choose'),
                            filterable: 'all',
                            multiple: true
                        })
                    ]
                });

                // When a Image is selected, run a callback.
                list_gallery_frame.on('select', function () {
                    var selection = list_gallery_frame.state().get('selection');
                    var attachment_ids = $images_id.val();

                    selection.map(function (attachment) {
                        attachment = attachment.toJSON();

                        if (attachment.id) {
                            attachment_ids = attachment_ids ? attachment_ids + ',' + attachment.id : attachment.id;
                            var attachment_image = attachment.sizes && attachment.sizes.thumbnail ? attachment.sizes.thumbnail.url : attachment.url;
                            $list_images.append('<li class="image" data-attachment_id="' + attachment.id + '"><img src="' + attachment_image + '" /><a href="#" class="delete" title="' + $el.data('delete') + '"><i class="fa fa-times" aria-hidden="true"></i> </a>  </li>');
                        }
                    });

                    $images_id.val(attachment_ids);
                });

                // Finally, Open the Modal.
                list_gallery_frame.open();
            });

            // Image Ordering
//            $list_images.sortable({
//                items: 'li.image',
//                cursor: 'move',
//                scrollSensitivity: 40,
//                forcePlaceholderSize: true,
//                forceHelperSize: false,
//                helper: 'clone',
//                opacity: 0.65,
//                placeholder: 'list-metabox-sortable-placeholder',
//                start: function (event, ui) {
//                    ui.item.css('background-color', '#f6f6f6');
//                },
//                stop: function (event, ui) {
//                    ui.item.removeAttr('style');
//                },
//                update: function () {
//                    var attachment_ids = '';
//
//                    $list_images_container.find('li.image').css('cursor', 'default').each(function () {
//                        var attachment_id = jQuery(this).attr('data-attachment_id');
//                        attachment_ids = attachment_ids + attachment_id + ',';
//                    });
//
//                    $images_id.val(attachment_ids);
//                }
//            });

            // Remove Images
            $list_images_container.on('click', 'a.delete', function () {
                $(this).closest('li.image').remove();

                var attachment_ids = '';

                $list_images_container.find('li.image').css('cursor', 'default').each(function () {
                    var attachment_id = jQuery(this).attr('data-attachment_id');
                    attachment_ids = attachment_ids + attachment_id + ',';
                });

                $images_id.val(attachment_ids);

                return false;
            });
        
        }
        // Setting List Fields onChange
        window.list_group_fields = function (list_group_id, list_id, admin_url) {
            var group_options = $('#group_options');
            if ('' != list_group_id) {
                group_options.slideDown('slow');
            } else {
                group_options.slideUp('slow');
            }
        }
        
        // Setting List Group Fields onChange
        $(".list-switch [type=checkbox]").on('change', function () {
            var self = $(this);
            var aData = self.attr("data-switch");
            console.log($("." + aData));
            $("." + aData).toggleClass('list-fieldset-hide');
        });
    });
})(jQuery);